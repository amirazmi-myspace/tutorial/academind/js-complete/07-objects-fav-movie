const addMovieBtn = document.getElementById('add-movie-btn');
const searchBtn = document.getElementById('search-btn');

const movies = [];
const renderMovies = (filter = '') => {
    const movieList = document.getElementById('movie-list');
    
    if (movies.length === 0) movieList.classList.remove('visible');
    else movieList.classList.add('visible');
    movieList.innerHTML = '';

    const filteredMovies = !filter ? movies : movies.filter((movie) => movie.info.title.toLowerCase().includes(filter));

    filteredMovies.forEach((movie) => { 
        const movieElement = document.createElement('li');
        if ('info' in movie) {
            console.log('yes has', movie.info);
        }
        const { info, getFormattedTitle } = movie;
        // getFormattedTitle = getFormattedTitle.bind(movie);

        let text = `${getFormattedTitle.apply(movie)} - `;
        for (const key in info) {
            if (key !== 'title') {
              text += `${key}: ${info[key]}`
          }
        }
        movieElement.textContent = text;
        movieList.append(movieElement);
    });
};

const addMovieHandler = () => {
    const title = document.getElementById('title').value.trim();
    const extraName = document.getElementById('extra-name').value.trim().toLowerCase();
    const extraValue = document.getElementById('extra-value').value.trim();

    if (!title || !extraName || !extraValue) return;

    const newMovie = {
        info: {
            title,
            [extraName]: extraValue,
        },
        id: Math.random().toString(),
        getFormattedTitle:() => {
            console.log(this)
            return this.info.title.toUpperCase();
        }
    };
    movies.push(newMovie);
    renderMovies();
};

const handler = function () {

    const yay = {
        test:'1'
    }

    const searchMovieHandler = () => {
        console.log(this.yay);
        const filterTerm = document.getElementById('filter-title').value.trim().toLowerCase();
        renderMovies(filterTerm);
    }
    searchMovieHandler();
}

addMovieBtn.addEventListener('click', addMovieHandler);
searchBtn.addEventListener('click', handler);
